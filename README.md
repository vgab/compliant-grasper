# Compliant Grasping Genius

![being sensitive to pressure](https://im.contentlounge.net/styles/manual_crop/s3/2019-01/close-up-finger-hurt-433625%20Cropped.jpg?im=Crop%2Csize%3D%28800%2C450%29%2Cgravity%3DCenter%3BBackgroundColor%2Ccolor%3Dtransparent&hash=a689ac6d62437a76ccdf7b82f7103148839ba3f053b53d3e47e95a4037fe3dca)

## Overview

- [```article```](./article) contains the article as LaTeX collection
- [```texmf```](./texmf) contains the custom / IEEE latex class and style-files
- [```pres_video```](./pres_video) contains the presentation video and slides
- [```presentation```](./presentation) contains the dedicated presentation-latex slides source
- [```reviews```](./reviews) contain the review comments
- [```upload_data```](./upload_data) contains the additional documents needed for RAS online uploads
- [```video```](./video) contains supplementary video material

## Notes on LaTeX Compilation

This setup requires to run latexmk or a proper configuration of your setup.

Either set the texmf directory as your current home, cf one of the links below:

- [stackoverflow->texstudio and local texmf](https://tex.stackexchange.com/questions/386166/texstudio-and-local-texmf)
- [stackoverflow->How to make LaTeX see local texmf tree](https://tex.stackexchange.com/questions/30494/how-to-make-latex-see-local-texmf-tree)
- [stackoverflow->TEXMFHOME setting](https://tex.stackexchange.com/questions/71469/texmfhome-setting)

Alternatively, simply use the [latexmk syntax](https://mg.readthedocs.io/latexmk.html)

```bash
cd article
latexmk -f -cd main.tex
```

for the article and

```bash
cd presentation
latexmk -cd -f final.tex
```

for the presentation.

Tested on:

Ubuntu 20.04 Gnome using [okular](https://okular.kde.org/de/) and [pympress](https://github.com/Cimbali/pympress/)

> **NOTE: both compilation procedures will start a viewer, either [okular](https://okular.kde.org/de/) for the article and [pympress](https://github.com/Cimbali/pympress/). The current setup assumes these programs to be installed**
