We wish to submit an original research article entitled
"A Force-Sensitive Grasping Controller Using Tactile Gripper Fingers
and an Industrial Position-Controlled Robot"
for consideration by the IEEE Robotics & Automation Letters.
We confirm that this work is original and has not been published elsewhere,
nor is it currently under consideration for publication elsewhere.
We have no conflicts of interest to disclose.

In this manuscript, we present a novel grasping controller that allows to apply
Industrial stiff position-controlled robots to be applied for adaptive grasping.
This research is significant to the community of IEEE Robotics & Automation letters
as there exist a broad variety of applications and problems, where
the perception of the environment is subject to sensor-noise,
such that a robot is required to compensate
for the resulting errors by means of compliant behavior.
Even though a variety of research exists, this problem has not yet been
solved to full satisfaction.
Especially if a rigid position-controlled robot platform is used,
only few solutions exists.
Our approach uses off-the-shelf hardware and increases the skill-set of industrial
robots by the ability to react to finger-contact, which we highlight on
a disassembly scenario in our manuscript as well as in the attached video.
We further provide the core content of this research as open-source software,
thus allowing future research to profit from our findings.

Please address all correspondence concerning this manuscript to me at v.gabler@tum.de.
Thank you for your consideration of this manuscript.
Sincerely,
Volker Gabler