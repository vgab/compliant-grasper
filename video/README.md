NOTE: this file is formated in markdown for eased readability.
As only .txt-files are accepted via papercept, the format has been adjusted.

The media file ``ICRA22_gabler_grasp_SD`` is generated to meet limits
of a ICRA-multimedia attachment according to [ICRA2022.org](https://icra2022.org/contribute/call-for-papers).

## Video Description

Video is (hopefully) self-explanatory, but the content is summarized as

- outlining the general gripper driver and digital sensor-reading
- introducing different control modalities and communication protocols via free-space human guidance
- exemplary video recording of experimental validation as outlined in the manuscript:
  1. Compliant strategy to grasp lightbulb
  2. MPC-based strategy to grasp lightbulb
- video ends with successful removal of lightbulb for MPC-based strategy

## Player Information

This Video has been tested / played via

- Mac OS:
  - [Quicktime player](https://support.apple.com/downloads/quicktime)
- Linux Ubuntu 20.04:
  - [Gnome video player](https://wiki.gnome.org/Apps/Videos/)
  - [VLC](https://www.videolan.org/vlc/download-ubuntu.html)

### Video Settings / Encoding details

All videos have been generated with Adobe Premiere, using predefined
default settings as listed below.

| Setting        | Value        |
| -------------- | ------------ |
| Format         | MPEG-4       |
| Duration       | 1:40 minutes |
| Codec          | H.264        |
| Framerate      | 25           |
| Audio          | No           |
| Scan type      | Progressive  |
| Resolution     | 480p         |
| Target Bitrate | 1.6          |
| Size           | 19.7MB       |

## Author Contact Information

- Name: Volker Gabler
- E-Mail: v.gabler@tum.de
