# Authors` Response

## Preliminary Notes

In order to ease the response, we have structured the feedback by the reviewer into categories to ease the structure of this response file. For brevity we omit responses to the summaries and positive feedback as there are neither any objections from our side, nor changes in the revised article.
In the following, we use the following reviewer-references:

* Review162745(Reviewer2) as **R2**
* Review162755(Reviewer7) as **R7**
* Review162759(Reviewer9) as **R9**

> **Please note: In order to ease transfer between the responses listed below and the revised manuscript,
> we highlighted the revised content in orange**
<!-- > changed content of the revised manuscript is grayed out. -->

## Writing / Style / Presentation

> **R2**
>
> 1. Redundancies or grammatical errors in a few places
> 2. In Section III B, it was unclear whether the two modalities were and the same as the two strategies; either way, it would be clearer to explain what they were before the explanation of the differences between your controller and the classic hybrid force-position controller
>
>
> **R7**
>
> 1. The url provided for the ros package is missing a colon after http.
> 2. The use of variable t for both tool and time can be confusing sometimes.
> 3. Same notation is used for specifying robot configuration and coordinate transformation which can be confusing.

First, we want to thank the reviewers for their valid concerns and pointing out the deficiencies in our initial contributions. We agree that these comments, as well as our adjustments improve the overall quality of the article and help to improve the understanding of the presented methods.
In detail we changed:

* **R2** &rarr; 1. additional proofreading and spell-checking
* **R2** &rarr; 2. Section III-B has been rearranged and rephrased in order to ease the golden threat.
* **R7** &rarr; 1. colon added to url
* **R7** &rarr; 2. we introduced color-encoding as well as a different font for reference frames `body`, `tool` and `end-effector` to clearly distinguish between `time` and `tool`.
* **R7** &rarr; 3. the notation of the robot forward kinematics has now been changed to $`\mathbf{x}`$.
**COMMENT: I would consider indices \text{left, right, tool} for better readability**
## Technical Details

> **R2**
>
> 1. Did not explain enough why compliant manipulators cannot be used in  environments that are not well-defined.
> 2. How could this be extended to more complex grippers (e.g with more than 2 fingers or multi-jointed fingers)?
> 3. It would have been useful to compare the controller performance to other controllers attempting to perform the same tasks

We thank the reviewer for raising these issues, as our initial submission was missing  not stating the application / problem setting of our method clearly. We addressed them as follows:

1. The issue we intend to address is allowing a compliant grasping strategy for a robot that does not allow for compliant control interfaces, such as impedance- / admittance- / torque-control. Instead, we use an industrial robot. We have adjusted this in the introduction (first two paragraphs) and the problem formulation section.
2. This is a valid question that we have added as a concluding discussion of III-B-2, as it was not possible to collect physical evidence on this matter.
3. This issue is discussed in the experimental section below, specifically in the [Additional Experimental Evaluation and Recordings](#additional-experimental-evaluation-and-recordings) paragraph.

---
> **R7**
>
> 1. It is not clear whether this approach works only on the object used in the experiment or can be used on other objects as well.
> 2. The assumption of a linear relationship between alignment error and control command used for MPC might not hold for objects of different shapes.
> 3. Also it is not obvious that reduction in external forces will always lead to reduction in alignment error.
> 4. The technical approach mentions a threshold on alignment error to switch between different controllers. But it is not mentioned in the experiment section what is the value of that threshold. Also it is not clear whether that threshold needs to be adjusted for every different type of object. It would be beneficial if authors could provide some discussion on how this threshold is selected.
> 5. equation (1) implies that the end effector pose error is
also dependent on external force. However from later sections, it seems
that this error is only dependent on pressure sensor readings.
> 6. Similarly, in equation (3), left hand side is a point and should be a
2d vector. But rhs appears to be a scalar quantity. It would be easier
to understand equation (3) if the term ij x2 is properly defined.

We thank the reviewer for the valuable feedback on the technical insights of our manuscript.
We revised our article in detail w.r.t. their comments in detail:

1. As this concern is a valid impression stemming from the presented experiments, we refer to the [Additional Experimental Evaluation and Recordings](#additional-experimental-evaluation-and-recordings) paragraph of this file below, for further details.
2. The submitted manuscript seems to not clearly state the relation of the alignment-error and the robot pose $`\mathbf{x}`$. As the desired pose $`\mathbf{x}_{\mathrm{des}}`$ is kept constant for the MPC-horizon $`T_{\mathrm{max}}`$, the linearization of the system model only relies on a locally linear behavior of the robot motion. Given the end-effector dynamics from (12) and a bounded velocity command $`\mathbf{u}_t`$, the alignment error can be locally linearized. We adjusted this within the explanation in III-B-2.
3. This is a valid issue that was not clearly outlined in the initial submission. We have thus rephrased the assumptions for the force-based strategy in III-B-1.
4. We agree with the reviewer. The minimum value for ``success`` in the initial version of the experimental section should have been denoted as $`\iota`$. We added additional notes on $`\iota`$ in III-B-1 as well as in the experimental section.
5. The reviewer is correct and has been adjusted in the problem-formulation, i.e. in (1) and (2).
6. The cell-center vectors have been introduced incorrectly in our initial manuscript. Further, the denominator was integrating over the cell-centers instead over the pressure reading. We have updated formula (3) accordingly.

---

> **R9**
>
> 1. Neither from the paper nor the video could I find how the adaptive strategy is implemented in detail. They look like two separate controllers. It is suggested more illustrations could be given on how to achieve the adaptive strategy.
> 2. Another question is that the paper assumes that both fingers are in contact with an object, then what if that the tactile information might not be collected in one finger. As the tactile data acquirement largely depends on the location and shape of the object.

We thank the reviewer for their concerns on the clarity of our manuscript regarding the technical insights. We adjusted their comments in detail as follows:

1. This impression presumably stems from the misleading Figure 4 and the structure of the overall text. We hope that the revised version of Figure 4 resolves this issue. As shown in the Figure, the control commands from the two strategies (velocity - red vs. force-based - green) are obtained independently. In our initial submission, these commands were then forwarded to the robot, so these strategies were applied separately.
In the revised manuscript we incorporated the feedback from the reviewers and thus outlined how a hybrid controller can be implemented that allows to apply a velocity- and a force-based strategy in complementing Cartesian directions.
This has also been added to the experimental section.
Finally, we have to admit that the term "adaptive" may be misleading here, as our approach is not referring to adaptive control, and thus the update of the controller parameterization, but rather an 'adaptive grasping strategy' from an application point of view
2. This concern is valid, but results from the missing arguments in the introduction and problem statement, and seems closely related to the issue from **R2** &rarr;1,2. Thus, the overall approach assumes that the alignment phase can only be executed during initial contact, i.e. at least one finger has non-empty sensor-data. Otherwise, (1) would need to be to regressed from the robot state, which is infeasible. In case, there is only contact at one finger, the alignment error is regressed from (11). We think that our adjustments in the problem formulation and introduction clarifies the comment.


## Multimedia Attachment

Based on the comments made by the reviewers we kept the multimedia unchanged and revised the manuscript instead.
Assuming that the revised manuscript answers the questions and misunderstanding to a satisfactory level,
we are confident that the provided video material supports the idea presented in this work in a suitable manner.

## Experimental Evaluation

> **R2**
>
> 1. It would have been useful to compare the controller performance to other controllers attempting to perform the same tasks.

We thank the reviewer for the valuable feedback. As this concern was raised by all reviewers, we refer to the [Additional Experimental Evaluation and Recordings](#additional-experimental-evaluation-and-recordings) paragraph below.

---

> **R7**
>
> 1. From the experiments, it is not very clear whether the authors are using a hybrid controller which is switching between the three strategies or just trying one strategy at a time.
> 2. It seems that the approach presented by the authors is velocity-based control but it has worse performance as compared to force-based control.
> 3. It is not clear what to conclude from these results.
> 4. Why different number of trials (11 vs 23) are used for velocity-based and force-based strategies?
> 5. The experiment has been done only on a single tube object. And the
success rate of only velocity-based and force-based controllers
proposed in this paper has been presented. Thus it is not clear how this controller would perform as compared to existing state of the art approaches like GPD or dexnet with open loop grasp execution
> Further, it is unclear how this controller would perform for different types of objects.

We want to thank the reviewers for their valuable feedback. We adjusted the experimental section and ran additional experiments. Concerning their feedback the individual adjustments are:

1. The concern raised by the reviewer is valid as our initial submission was lacking in clarity about the hybrid controller and the relation to the actual grasping strategies.
In fact, our initial experiments were solely evaluating each strategy separately.
Due to the concerns raised by the reviewers, we extended our method by a hybrid strategy that is outlined in III-B-3 and evaluated in the additional experiments.
2. This seems like a result from the concern above and below. We hope that the rephrased section III-B gives a clearer idea of the actual robot controller implementation.
Both of these grasping strategies use a velocity-control interface with the robot.
Using a hybrid controller allows to switch between selective grasping strategies (c.f. [Additional Experimental Evaluation and Recordings](#additional-experimental-evaluation-and-recordings)).
3. We have added an additional paragraph on the discussion of the collected empirical data an revised the experimental section (c.f. [Additional Experimental Evaluation and Recordings](#additional-experimental-evaluation-and-recordings)).
4. This is a valid concern that has been adjusted in our revised manuscript and experimental recordings (c.f. [Additional Experimental Evaluation and Recordings](#additional-experimental-evaluation-and-recordings)).
5. As this concern was raised by all reviewers, we refer to the [Additional Experimental Evaluation and Recordings](#additional-experimental-evaluation-and-recordings) paragraph below. Nonetheless, we omitted to explicitly mentioning why state-of-the-art methods such as GPD or dexnet were not considered as comparable methods, as both of these approaches rely on point-cloud data. As outlined in the problem formulation and introduction, our method aims towards enabling robots to adaptive their grasp during initial contact / grasping motions, where no vision or point cloud data is available, e.g. grasping objects that are occluded and thus can not be detected by a vision system.

---

> **R9**
>
> It is mentioned in the paper that there are also some other methods using force-based approaches. Could the authors provide more discussion on the comparison between the proposed method and the other force-based method in aspects such as performance, efficiency, etc.

We thank the reviewer for the valuable feedback. As this concern was raised by all reviewers, we refer to the [Additional Experimental Evaluation and Recordings](#additional-experimental-evaluation-and-recordings) paragraph below.

### Additional Experimental Evaluation and Recordings

We have adjusted the experimental evaluation of the presented grasping controller, in order to address the issues raised by the reviewers.
In detail:

1. we revised the experimental description such that the motivation and assumption are laid out clearly (c.f. IV-B). Specifically, we revised the description of the experimental setup to ease understanding of the methods evaluated, as well as the experimental setup. We emphasize that our approach does not aim or comparison against vision based grasping pose detection / grasp synthesis but rather the compliant alignment control of the overall robot structure.
2. We kept the previous recording on the lightbulb removal and used these results to explicitly discuss the differences of the proposed grasping strategies from (III-B-1/2) (c.f. IV-B-2). We removed the evaluation of the success-rate as this has been shifter to a proper comparison against the baseline method in IV-B-1. Instead, we focused the discussion on the resulting wrench plots and use these results to highlight the differences of the presented methods.
3. As stated before, we ran additional recordings on alternative objects in the new section IV-B-a and table II. In this section we also added a baseline comparison to evaluate the improvements of our method.
4. we revised the discussion of previous empirical data recordings (IV-B-2) and added a new outline in (IV-B-1). We are confident that the revised experimental discussions underline our contribution more clearly and increase the overall quality of the paper distinctly.
5. thanks to the comments made by the reviewers, we also extended the experiment by two hybrid control strategy that use a mixture of the proposed strategies depending and added the evaluation to IV-B-1 and table II.
