# Review Summary

## Progress Report

### Writing / Presentation

* [ ] **R2** Redundancies or grammatical errors in a few places
* [ ] **R2** In Section III B, it was unclear whether the two modalities were and the same as the two strategies; either way, it would be clearer to explain what they were before the explanation of the differences between your controller and the classic hybrid force-position controller
* [x] **R7** The use of variable t for both tool and time can be confusing sometimes.
* [x] **R7** The url provided for the ros package is missing a colon after http.
* [x] **R7** Same notation is used for specifying robot configuration and coordinate transformation which can be confusing.

### Technical Approach / Technical Issues

* [ ] (**R2**) It would have been useful to **compare the controller performance to other controllers attempting to perform the same tasks**
* [ ] (**R7**) It is **not clear whether this approach works only on the object used in the experiment or can be used on other objects as well**.
* [ ] (**R7**) The assumption of a **linear relationship between alignment error and control command used for MPC might not hold for objects of different shapes**.
* [ ] (**R7**) Also it is **not obvious that reduction in external forces will always lead to reduction in alignment error**.
* [ ] (**R7**) The technical approach mentions a threshold on alignment error to
switch between different controllers. **But it is not mentioned in the experiment section what is the value of that threshold.**
* [ ] (**R7**) Also it is **not clear whether that threshold needs to be adjusted for every different type of object**. It would be beneficial if authors could provide some discussion on how this threshold is selected.
* [ ] (**R9**) Neither from the paper nor the video could I find how the adaptive strategy is implemented in detail. They look like two separate controllers. It is suggested **more illustrations could be given on how to achieve the adaptive strategy**.
* [ ] (**R9**) Another question is that the paper assumes that both fingers are in contact with an object, then **what if that the tactile information might not be collected in one finger**. As the tactile data acquirement largely depends on the location and shape of the object.

### Experimental concerns

* [ ] (**R2**) It would have been useful to **compare the controller performance to other controllers attempting to perform the same tasks**
* [ ] (**R7**) From the experiments, it is **not very clear whether the authors are using a hybrid controller which is switching between the three strategies or just trying one strategy at a time.**
* [ ] (**R7**) It seems that the approach presented by the authors is velocity-based control but it has worse performance as compared to force-based control.
* [ ] (**R7**) It is not clear what to conclude from these results.
* [ ] (**R7**) Why different number of trials (11 vs 23) are used for velocity-based and force-based strategies?
* (**R7**) The experiment has been done only on a single tube object. And the
success rate of only velocity-based and force-based controllers
proposed in this paper has been presented. Thus it is not clear:
  * [ ] how this controller would perform as compared to existing state of the art approaches like GPD or dexnet with open loop grasp execution
  * [ ] how this controller would perform for different types of objects.
* [ ] (**R9**) It is mentioned in the paper that there are also some other methods using force-based approaches. **Could the authors provide more discussion on the comparison between the proposed method and the other force-based method in aspects such as performance, efficiency, etc.**

### Questions and comments

* [ ] (**R2**) Did not explain enough why compliant manipulators cannot be used in environments that are not well-defined.
* [ ] (**R2**) How could this be extended to more complex grippers (e.g with more than 2 fingers or multi-jointed fingers)?

#### Multimedia-Critic

* (**R2**) Video could have been more detailed in explanation of how it worked

## positive concerns / feedback

* A controller and implementation for a feedback controller for grasping using a system with digital sensor arrays (**R2**)
* In this paper, the authors propose a force-sensitive grasping controller which can estimate the alignment error when interacting with the object. Both the  velocity-based and tactile-based grasping strategies are investigated in the disassembly task. (**R9**)
* This paper presents a controller which can use the difference in desired and current  center of pressure on the digital sensor array on the left and right fingers of the non-compliant robot gripper to adjust the gripper pose for better grasping. (**R7**)

### Writing

* (**R2**) The text of the paper is generally well organized and proceeds logically
* (**R2**) This paper is presented well, and very clearly states its
contributions and explains them well .
* (**R2**) Citations were relevant to the worked described in this paper .
* (**R9**) Citations seem to cover the existing work well.

## Multimedia

* (**R2**) Video showed a good demonstration of controller
* (**R9**) The video is informative to show the performance of the controller.
* (**R7**) The attached video is well made. It shows one trial of velocity-based strategy and force-based strategy and highlights how such strategies can be helpful in case of a tube object.
