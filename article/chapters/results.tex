\section{Experiment}\label{sec:Grasp:experiment}
In this section we evaluate the proposed ROS-driver and grasping
strategies. For convenience, bold numbers emphasize improved measurements in the tables.

\subsection{Evaluating Communication Speed}\label{sec:Grasp:experiment:communication}
The extended WSG 50 ROS-driver provides two communication modes,
to allow for~\ac{DSA}-readings: the full~\ac{DSA} data (\SI{336}{\byte}),
and a sparse-message that only contains the~\acp{CoP} and cell numbers of the edge of
the pressure array (\SI{21}{\byte} to \SI{62}{\byte}).
In order to compare the different communication modes of the proposed gripper driver,
empirical data has been collected over $50$ grasping trials in in~\autoref{tab:Grasp:experiment:ros-hz}
setting the desired update rate to~\SI{10}{\hertz}.
As both methods only send~\ac{DSA}-data during contact,
the average communication speed is close to \SI{10}{\hertz}.
The last column presents the achieved communication rates during contact,
where a stable communication rate of \SI{5.49}{\hertz} was measured.
\begin{table}[b]
    \centering
    \vspace{-0.2cm}
    \CompactCaption{
        Average~\ac{ROS}-update rate in \SI{}{\hertz}.
    }
    \begin{small}
        \label{tab:Grasp:experiment:ros-hz}
        \begin{tabular}{l| ccc }
            \toprule
            {\bf Communication-mode}                   & {$\sMean[\text{rate}]$} & {$\Variance[\text{rate}]$} & {$\sMean[\text{contact}]$} \\
            \midrule
            full~\ac{DSA} read [\SI{}{\hertz}]   & 9.08                    & 1.66                       & 4.50                       \\
            sparse~\ac{DSA} read [\SI{}{\hertz}] & {\bf 9.62}              & {\bf 1.25}                 & {\bf 5.49}                 \\
            \bottomrule
        \end{tabular}
    \end{small}
\end{table}
% %


\subsection{Evaluation of Proposed Grasping Strategies}
\label{sec:Grasp:experiment:grasping-benchmark}
\begin{figure}[t]
    \vspace{0.3cm}
    \centering
    \includegraphics[width=\linewidth]{./pics/grasper/experiment_final.png}
    \vspace{-2pt}
    \CompactCaption{
        The left shows the objects used for the benchmark test against a baseline method,
        whereas the right shows a an examplary
        task of removing a lightbulb,
        on which we compare the strategies
        from~\autoref{sec:Grasp:approach:robot-control:force} and~\autoref{sec:Grasp:approach:robot-control:velocity}.
        For each object,
        the robot receives a noisy goal-pose, which needs to be compensated
        solely from \ac{FT}-sensor data and~\acp{DSA} readings.
    }\label{fig:Grasp:experiment:setup}
    \vspace{-0.2cm}
\end{figure}
In order to highlight the functionality of the proposed framework,
we evaluate the grasping strategies on exemplary object grasping tasks
using a COMAU Racer 5 0.80 robot as depicted in~\autoref{fig:Grasp:experiment:setup}.
Recalling the main motivation of this work, the robot has no access to
visual or depth cameras and needs to grasp the objects
given a noisy goal poses.
Thus, each grasping trial directly starts at the noisy goal pose with the gripper set to the maximum
opening width.
The recording starts by initiating the gripper closure with closing speed $v_{\mathrm{gripper}}$,
and compensating for the pose-error, \eg/,~according to~\autoref{sec:Grasp:technical-approach}.
As the~\ac{DSA}-sensors are sensitive to slipping forces,
evaluating a trial as successful if the object is lifted successfully, remains risky.
Thus, we evaluated the \SE/ pose-error against $\iota=\SI{0.5}{\milli\meter}$ in translation
and $\iota=\SI{1e-2}{\radian}$ in rotation
from~\autoref{sec:Grasp:approach:robot-control:force}.
Consequently, these values also formed the thresholds to enable the
velocity- and force-based grasping strategies
from~\autoref{sec:Grasp:approach:robot-control:force}.
For a fair comparison, all methods were given the identical $10$ goal poses for each object.
\\\noindent
We set $v_{\mathrm{gripper}}$
to \SI{10}{\milli\meter\per\second}
and $\vv{v}[\max]$ to \SI{5}{\milli\meter\per\second}
in~\eqref{eq:Grasp:approach:vel-ctrl}.
The proportional gains in~\eqref{eq:Grasp:approach:force-control:PI}
and~\eqref{eq:Grasp:approach:sensor-track:hybrid-sns-ctrl}
were
$\GainMat[P][\Fx] = \diag{\vecTranspose{
    \num{8e-4} \ones[3], \num{2e-3}\ones[3]
}}$.
In addition, we set $N_I=50$ and $\GainMat[I][\Fx]=\num{1e-6} \ones[6][6]$
for the integral term in~\eqref{eq:Grasp:approach:force-control:PI}.
For the velocity-based strategy we applied $\Mat{C}[Q] = \ones[6][6]$ and
$\Mat{C}[R]=\num{1e-4}\ones[6][6]$ and used~\cite{SWCasADi,SWIpOpt,SWDoMPC}
to solve~\eqref{eq:Grasp:approach:vel-ctrl}.

\subsubsection{Comparison against Baseline}
\begin{table*}[t]

    \centering
    \CompactCaption{
        Collected experimental data for four variants of the proposed
        methods compared against a baseline solution, \ie/,~a compliant robot.
        The norm-values for the alignment error and interaction wrench per step
        are averaged over $10$ grasping-trials for each of the objects from
        \autoref{fig:Grasp:experiment:setup}.
        The last column shows the overall success-ratio for all objects per method.
    }
    \begin{small}
	\label{tab:Grasp:experiment:grasping-result}
		\begin{tabular}{l| cc | cc | cc | cc | c}
			\toprule
			{\bf Object} &  \multicolumn{2}{c}{Pocket Ruler} &
			                \multicolumn{2}{c}{Hammer} &
                            \multicolumn{2}{c}{Electrical Drill} &
                            \multicolumn{2}{c}{Cooking Pan} & \\
			{\bf Strategy}
                    & {$\sMean[\gls{ErrorVec}]$}  & {$\sMean[\Fx]$}
                    & {$\sMean[\gls{ErrorVec}]$}  & {$\sMean[\Fx]$}
                    & {$\sMean[\gls{ErrorVec}]$}  & {$\sMean[\Fx]$}
                    & {$\sMean[\gls{ErrorVec}]$}  & {$\sMean[\Fx]$}
                    & $p_{\mathrm{success}}$
                         \\
			\midrule
			force-based  & 1.576 & 12.34
			             & 2.118 & 18.93
			             & 0.526 & 17.92
                         & 0.797 & {10.36}
                         & 41.17\%\\
			vel-based    & {\bf 1.216} & 54.98
			             & {\bf 1.484} & 55.62
			             & {\bf 0.376} & {60.25}
                         & {\bf 0.615} & {28.79}
                         & 77.58\%\\
            hybrid-v-f   & 1.567 & 64.02
			             & 2.039 & 20.68
			             & 0.576 & {25.73}
                         & 0.832 & {10.21}
                         & 22.54\%\\
			hybrid-f-v   & 1.597 & 16.56
			             & 1.936 & {45.38}
			             & 0.323 & {48.77}
                         & 0.627 & {22.05}
                         & {\bf 84.17\%}\\
			baseline     & 1.606 & {\bf 8.98}
			             & 2.249 & {\bf 8.83}
			             & 0.647 & {\bf 4.30}
                         & 0.855 & {\bf 5.591}
                         & 9.55\%\\
		\bottomrule
		\end{tabular}
	\end{small}
    \vspace{-0.5cm}
\end{table*}
As shown in~\autoref{tab:Grasp:experiment:grasping-result}, we empirically evaluated
our method
against a baseline method
by grasping the objects on the left hand-side of~\autoref{fig:Grasp:experiment:setup}.
We employed a solely compliant robot as a `baseline`' by setting
$\GainMat[I][\Fx] = \zeros[6][6]$,
$\Fxd = \zeros[6]$
and $\sca{\gls{SelectionElem}}[k][\Fx] = 1$
in~\eqref{eq:Grasp:approach:sensor-track:hybrid-sns-ctrl}.
In addition, we used four variants of our approach:
The `{force-based}` and `{vel-based}` directly forward the commands obtained
from~\autoref{sec:Grasp:approach:robot-control:force}
and~\autoref{sec:Grasp:approach:robot-control:velocity},
while `{hybrid-f-v}` and `{hybrid-v-f}` apply a hybrid strategy along
complementary Cartesian directions.
In detail, `{hybrid-f-v}` follows the force-based strategy in translation and
the velocity-based strategy for the rotation,
while `{hybrid-v-f}` reverts this scheme.
As the presented methods rely on encountered interaction wrenches and/or
sensor readings from the~\acp{DSA}, which result from said interaction wrenches,
the objects from~\autoref{fig:Grasp:experiment:setup} were fixed to the table
in front of the robot.
Each experimental run lasted $2000$ steps with an update rate of \SI{100}{\hertz}
resulting in the averaged results listed in~\autoref{tab:Grasp:experiment:grasping-result}.
A task is set as successful if the final error is below $\iota$.
As our strategies are non-compliant if there is no contact or feedback
obtained from the gripper, which inherently suffers from the communication
delay due the decreased update rate,
the baseline method outperforms our
approaches in terms of overall interaction wrenches.
Nonetheless, the baseline solution fails on solving the task and remains
stuck at unacceptable final error residuals.
Even though, the velocity based strategy performs best \wrt/~the overall
alignment error, this strategy still suffers from large interaction wrenches.
Given that the hybrid strategy performs best in terms of success-ratio,
we propose that our method is best to be applied by its' hybrid nature.
Adding feasible upper wrench constraints can thus be used to apply the
velocity-based control strategy if possible and switch to a compliant control
otherwise.

\subsubsection{Cross-Comparison of the Presented Grasping Strategies}%
\label{sec:Grasp:experiment:grasp-lightbulb}
\begin{figure}[t]
    \vspace{0.3cm}
    \begin{subfigure}{0.95\linewidth}
        \input{tikz/figs/forces_xy.tex}
    \end{subfigure}
    \begin{subfigure}{0.95\linewidth}
        \input{tikz/figs/torque_z.tex}
    \end{subfigure}
   \CompactCaption{
        Cartesian wrench profiles for the lightbulb removal task
        averaged over $10$ trials for the `vel-based` and the `force-based`'
        strategies.
    }\label{fig:Grasp:experiment:lightbulb:wrenches}
    \vspace{-0.6cm}
\end{figure}

In this section we explicitly compare the presented strategies
from~\autoref{sec:Grasp:approach:robot-control:force} and
\autoref{sec:Grasp:approach:robot-control:velocity}
based on
the removal of an emergency lightbulb
as presented in the accompanied media attachment.
The resulting force-profiles in $xy$-plane of the robot
as well as the Cartesian torque in $\psi$ are sketched
in~\autoref{fig:Grasp:experiment:lightbulb:wrenches}.
Due to the translational offset, the robot encounters contact with the object on one finger,
which results in an increased force-peak for both strategies.
While the~\ac{MPC} controller is tuned well, and adjusts the velocity to the
current closing speed of the gripper, the force remains limited in this stage.
In contrast, the compliant force-controller suffers from the induced temporal delay
due to the~\ac{DSA}-readings, which allows to compensate for the current force-error
but results in an error-residual while being dragged across the workspace by the
lightbulb.
In contrast, the force-controller successfully steers the Cartesian wrench to zero
and thus also compensates the attitude error in $\psi$,
while the~\ac{MPC}-controller focuses on the positional error.
Even though this error is minimized around this point, there remains a static
error-residual in Cartesian torque.

