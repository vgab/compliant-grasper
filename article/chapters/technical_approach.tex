\section{Technical Approach}\label{sec:Grasp:technical-approach}

According to the problem from~\autoref{sec:Grasp:problem-description},
our approach is outlined sequentially.
First, we show how the \ee/~alignment error is estimated from
tactile sensor-arrays.
Given this, we outline the proposed grasping strategies and 
how they can be embedded on an industrial robot 
in order to steer the robot
to $\vxd$.

\subsection{Alignment Error Estimation Using Tactile Sensor Arrays}
\label{sec:Grasp:approach:alignment-estimation}
\begin{figure}[t]
    \vspace{0.5cm}
    \centering
        \resizebox {\linewidth}{!} {
        \begin{tikzpicture}
           \input{tikz/wsg_styles}
           \input{tikz/wsg_dsa_mat_empty}
           \input{tikz/wsg_dsa_f0.tex}
        \end{tikzpicture}
           }
    \CompactCaption{\ac{DSA} cells
        with exemplary pressure data for~\gls{DSAL}.
        Colors encode the coordinate-system axes
        \gls{rviz_x}, \gls{rviz_y} and \gls{rviz_z}.
        In here a toy-pressure distribution as a grey ellipsoid is shown,
        that results in the colored pressure distribution.
        $\vv[\DSAL]{r}[\CoPDes]$ is the center of the array and
        assumed to be the desired~\acs{CoP} here,
        while $\vv[\DSAL]{r}_{\text{\acs{CoP}}}$ is obtained
        from~\eqref{eq:Grasp:approach:dsa:cop}.
    }\label{fig:Grasp:approach:dsa:cell-array}
    \vspace{-0.5cm}
\end{figure}

The WSG 50 \ac{DSA} sensor-arrays allow a robot to record a \SI{12}{\bit} encoded sensor-cell
matrix reading of a $14\times 6$ sized array,
\ie/,~$\gls{DSAL} \in \NA[14][6]$.
Introducing frames $\DSAL, \DSAR$ for the left and right~\ac{DSA}-fingers,
as well as cell-sizes $\Delta_{x}$ according to~\autoref{fig:Grasp:approach:dsa:cell-array},
the center location $\vv[\DSAL]{r}_{ij}$ of each cell-element
$\elem{\gls{DSAL}}{i,j}$ can be expressed in the
respective finger reference frame $\vv[\DSAL]{r}_{ij}=
\begin{bmatrix} i\Delta_{x} & j\Delta_{x} & 0\end{bmatrix}\tr$.
As a result, the~\ac{CoP} is obtained as
\begin{equation}\label{eq:Grasp:approach:dsa:cop}
    \begin{aligned}
        \vv[\DSAL]{r}_{\text{\acs{CoP}}} &=
            \frac {\iintegral{\vv[\DSAL]{r}[x,y] \gls{DSAL}\func{x,y}}{x}{y}}
                  {\iintegral{\gls{DSAL}\func{x,y}}{x}{y}}\\
        &\approx \frac {\sum_{i=1}^{14}\sum_{j=1}^{6} \vv[\DSAL]{r}_{ij}
                                        \elem{\gls{DSAL}}{i,j}}
                       {\sum_{i=1}^{14}\sum_{j=1}^{6} \elem{\gls{DSAL}}{i,j}}.
    \end{aligned}
\end{equation}
The center of pressure is visualized in~\autoref{fig:Grasp:approach:dsa:cell-array}
for a fictional pressure surface and reading.
Assuming that both fingers are in contact with an object,
the unit-vector connecting the ~\acp{CoP} of both fingers
results in
\begin{equation}\label{eq:Grasp:approach:dsa:cop:line}
    \vv[\DSAR]{e}[\cur]
    =
    \frac 1\nu \func{
    \underbrace{
    \Transform{\DSAL}{\DSAR}
        \vv[\DSAL]{r}[\CoPL] - \vv[\DSAR]{r}[\CoPR]
    }_{\vv[\DSAR]{r}[\mathrm{\acs{CoP}},\cur]
    }},
\end{equation}
where $\nu$ is a normalizing constant.
The middle of the connecting vector
\begin{equation}\label{eq:Grasp:approach:dsa:cop:center}
    \vv[\toolFr]{r}[\CoPC] =
        \Transform{\DSAR}{\toolFr} \func{\vv[\DSAR]{r}[\CoPL]
         + \frac 12
         \vv[\DSAR]{r}[\mathrm{\acs{CoP}}, \cur]
    }
    ,
\end{equation}
in the tool-frame of the robot.
%
The transformation from $\DSAR$ to $\DSAL$
is given as
\begin{equation}\label{eq:Grasp:approach:dsa:cop:transform:F0_T_F1}
    \Transform{\DSAR}{\DSAL} =
    \begin{bmatrix}
        \gls{RRoll}\func{\pi}  &
            \begin{bmatrix}0\\\gls{DSAWidth}\\\gls{WSGwidth} \end{bmatrix} \\
        \zeros[1][3] & 1
    \end{bmatrix},
\end{equation}
where the current opening width \gls{WSGwidth} of the gripper is the only
time-variant value,
while $\gls{DSAWidth} = 6 \Delta_y$ denotes the width of the tactile sensor array
and the rotation matrix is a \SI{180}{\degree} flip around $\sca[\DSAR]{\gls{rviz_x}}$.
Similarly, the transformation from $\toolFr $ to $\DSAR$ is given as
\begin{equation}\label{eq:Grasp:approach:dsa:cop:transform:F0_T_T}
    \Transform{\toolFr}{\DSAR} =
    \begin{bmatrix}
        \gls{RPitch}\func{-\frac{\pi}{2}}  & \frac 12
            \begin{bmatrix}0\\\gls{DSAWidth}\\\gls{WSGwidth} \end{bmatrix} \\
        \zeros[1][3] & 1
    \end{bmatrix},
\end{equation}
with a rotation of \SI{-90}{\degree} around $y$.
In order to estimate the current alignment error, $\vv[\set{\DSAL{}, \DSAR{}}]{r}[\CoPDes]$,
the desired location of each~\ac{CoP} is assumed to be known, such that
$\vv[\DSAR]{e}[\cur]$ can be inferred.
Assuming that each~\ac{CoP} is located at the center of each finger, the
relation for an exemplary pressure distribution is shown
in~\autoref{fig:Grasp:approach:dsa:sensor-example}.
In this case, these~\acp{CoP} are identical to~\eqref{eq:Grasp:approach:dsa:cop}
if $\gls{DSAR}=\zeros[6][14]$ holds.
Transforming the unit-vectors into the tool-frame, \ie/,~$\vv[\toolFr]{e}[\gls{Dummy}]$,
with $\gls{Dummy} = \set{\des, \cur}$,
\begin{figure}[t]
    \vspace{0.5cm}
    \centering
    \resizebox{0.75\linewidth}{!}{
        \begin{tikzpicture}
            \input{tikz/wsg_styles}
            \input{tikz/wsg_dsa_fingers.tex}
        \end{tikzpicture}
    }
    \CompactCaption{Alignment error estimation visualization given the~\acp{CoP} in each finger,
    connected by a blue vector, while the green line denotes the vector connecting the
    desired~\acp{CoP}.
    The centers of both vectors and respective unit-vector are then used to estimate the
    alignment error according to~\eqref{eq:Grasp:approach:alignment:error}.
    }\label{fig:Grasp:approach:dsa:sensor-example}
    \vspace{-0.5cm}
\end{figure}
the alignment error is estimated as
\begin{align}\label{eq:Grasp:approach:alignment:error}
    \sca[\toolFr]{\gls{ErrorVec}} &:= \begin{bmatrix}
        \sca[\toolFr]{\gls{ErrorVec}}[\mathrm{trans}]\\
        \sca[\toolFr]{\gls{ErrorVec}}[\mathrm{rot}]\\
    \end{bmatrix}, \text{where} \\
    \label{eq:Grasp:approach:alignment:error:trans}
    \sca[\toolFr]{\gls{ErrorVec}}[\mathrm{trans}] &=
        \vv[\toolFr]{r}[\CoPDes] - \vv[\toolFr]{r}[\CoPC]
\end{align}
and the rotation error $\sca[\toolFr]{\gls{ErrorVec}}[\mathrm{rot}]$
defines the rotation needed to align $\vv[\toolFr]{e}[\cur]$
to $\vv[\toolFr]{e}[\des]$.
As there are infinite solutions due to the symmetry of the
unit-vectors around the $x$-axis, the $y$-axis can be either obtained
from matching the contours on both~\acp{DSA}, or projected into the $xy$-plane of $\toolFr$
via
$\sca[\toolFr]{\tilde{\gls{rviz_y}}}[\gls{Dummy}] = \brackets{\elem{\vv[\toolFr]{e}[\gls{Dummy}]}{y}, \sqrt{1.0 -\elem{\vv[\toolFr]{e}[\gls{Dummy}]}{y}^{2}}, 0.0}\tr$.
Using the direction of the unit-vectors as the $x$-axis, the dedicated $z$-axis
is obtained via the cross-product of
$
\vv[\toolFr]{e}[\gls{Dummy}]
$
 and $
\sca[\toolFr]{\tilde{\gls{rviz_y}}}$:
\begin{equation}\label{eq:Grasp:approach:alignment:error:rot}
    \begin{aligned}
    \sca[\toolFr]{\gls{ErrorVec}}[\mathrm{rot}]\hspace{-3pt}\gets\hspace{-3pt}
        \mathtt{RPY}
            &
            \left(
            \RotMat\func{
                \vv[\toolFr]{e}[\cur],
                \sca[\toolFr]{\tilde{\gls{rviz_y}}}[\cur],
                \vv[\toolFr]{e}[\cur] \times \sca[\toolFr]{\tilde{\gls{rviz_y}}}[\cur]
            }\tr
            \right.
                \\
            & \;
            \left.
             \RotMat\func{
                \vv[\toolFr]{e}[\des],
                \sca[\toolFr]{\tilde{\gls{rviz_y}}}[\des],
                \vv[\toolFr]{e}[\des] \times \sca[\toolFr]{\tilde{\gls{rviz_y}}}[\des]}
            \right)
    \end{aligned}
\end{equation}
where $\mathtt{RPY}$ denotes the mapping from rotation matrix to $\phi,\theta,\psi$ in order $zyx$,
and the individual rotation matrices are constructed from their individual unit-axes.
Finally, a special case is given if only one finger establishes contact with the object.
In such cases, the attitude error is set to zero in~\eqref{eq:Grasp:approach:alignment:error}
while~\eqref{eq:Grasp:approach:alignment:error:trans} is replaced by a heuristic component
\begin{equation}\label{eq:Grasp:approach:alignment:error:trans:single-contact}
    \sca[\toolFr]{\gls{ErrorVec}}[\mathrm{trans}]\hspace{-3pt}=\hspace{-3pt}
    \begin{cases}
        \Transform{\DSAL}{\toolFr} \begin{bmatrix}0& 0& \frac 12 \gls{WSGwidth}\end{bmatrix}\tr &\text{iff } \norm{2}{\gls{DSAL}} > 0 \\
        \Transform{\DSAR}{\toolFr} \begin{bmatrix}0& 0& \frac 12 \gls{WSGwidth}\end{bmatrix}\tr &\text{iff } \norm{2}{\gls{DSAR}} > 0 \\
        \zeros[3] & \text{else}.
    \end{cases}
\end{equation}

\subsection{Compliant Alignment Control for Industrial Robots}\label{sec:Grasp:approach:robot-control}
Having obtained the estimated alignment-error
as a Cartesian pose error in \SE/, the subsequent problem according to
\autoref{sec:Grasp:problem-description} is given by deploying a suitable
control strategy to minimize this error.
%
As most common grippers do not allow to control each finger separately,
the alignment control 
is preferably 
handled by the robot manipulator,
which is assumed to be an industrial robot manipulator with a restricted control interface -- \ie/,~controlling the Cartesian pose of the robot \ee/.
In order to achieve a sensitive aligment control, we propose two control-strategies,
which are schematically outlined in~\autoref{fig:Grasp:robot}:
a compliant force-based strategy that inorporates additional-\ac{FT}-sensor data readings,
as well as a~\ac{MPC} that generates a constrained Cartesian velocity-profile.
Defining the control-command $\vu$ as the Cartesian \ee/-velocity,
we outline these strategies below, followed by an insight on the actual
implementation on the robot platform.

\subsubsection{{\color{TUMdgreen}Force}-Based Grasping Strategy}\label{sec:Grasp:approach:robot-control:force}
This grasping strategy allows a compliant robot behavior towards external Cartesian wrenches,
via a PI-control
\begin{equation}\label{eq:Grasp:approach:force-control:PI}
    \begin{aligned}
    \vu[][{\color{TUMdgreen}F}, \mathrm{PI}] :=
    \GainMat[P][\Fx] 
        \underbrace{
        \func{\Fxd[]- \Fx[]}
            }_{\sca{\gls{ErrorVec}}[][\Fx]}
      +
    \GainMat[I][\Fx] \sum_{i=0}^{N_I} \Fxd\tm{i} - \Fx\tm{i},
    \end{aligned}
\end{equation}
using positive semi-definite control-gain matrices $\GainMat[P][\Fx]$ and $\GainMat[I][\Fx]$,
as well as a sliding window of size $N_I$ for numerical integration,
to steer the interaction wrench towards the desired value $\Fxd$.
Assuming the object to be grasped is quasi-static,
an alignment error during grasping results in a non-zero wrench measurement,
while the minimal interaction wrench is obtained when the robot is aligned correctly \wrt/~the object.
Thus, the desired Cartesian wrench is set to $\Fxd=\zeros[6]$.
In order to incorporate the alignment-error from~\autoref{sec:Grasp:approach:alignment-estimation},
the actual command forwarded to the robot is obtained as
\begin{align}\label{eq:Grasp:approach:force-control}
    \vu[][{\color{TUMdgreen}F}, \gls{Dummy}] &:=
    \sca{\gls{SelectionMat}}[\RotMat][\Fx]
    \vu[][{\color{TUMdgreen}F}, \mathrm{PI}, \gls{Dummy}], \text{with} \\
    \label{eq:Grasp:approach:force-control:selection-matrix}
    \sca{\gls{SelectionMat}}[\RotMat][\Fx] &:=
        \begin{bmatrix}
            \RotMat[\toolFr][\baseFr]
            \diag{\sca{\gls{SelectionElem}}[1:3]}
            \RotMat[\baseFr][\toolFr] & \zeros[3][3] \\
            \zeros[3][3] &
            \RotMat[\toolFr][\baseFr]
            \diag{\sca{\gls{SelectionElem}}[3:6]}
            \RotMat[\baseFr][\toolFr]
        \end{bmatrix}.
\end{align}
A binary selection signal \sca{\gls{SelectionElem}} is introduced \wrt/~$\toolFr$,
that is set to $1$, if $\norm{1}{\sca[\toolFr]{\gls{ErrorVec}}[k]} > \iota, $,
where $\iota$ is a non-negative threshold.
While $\iota$ is usually set to $0$, it forms an additional hyperparameter
that allows to neglect small alignment errors.
Recalling the integral term in~\eqref{eq:Grasp:approach:force-control:PI},
it has to be noted that this may lead to instability if contact dynamics are changing.
However, \eqref{eq:Grasp:approach:force-control}-\eqref{eq:Grasp:approach:force-control:selection-matrix}
allow to adjust the integral term during contact-changing events.
Therefore, not only the force-errors are stored in a sliding-window to calculate
the I-term in~\eqref{eq:Grasp:approach:force-control:PI}, but also the dedicated selection-matrices
$\sca{\gls{SelectionMat}}[\RotMat, t][\Fx]$.
If eventually a value of $\sca{\gls{SelectionElem}}[k]$ changes, all values of $k$ are set to zero.
Given that the obtained alignment error stems from a pressure distribution along the sensor cell arrays,
minimizing the force-control error along the selected axes via
the force-control term in~\eqref{eq:Grasp:approach:sensor-track:hybrid-sns-ctrl}
eventually minimizes $\sca[\toolFr]{\gls{ErrorVec}}[\eeFr]$ and thus also $\sca[\baseFr]{\gls{ErrorVec}}[\eeFr]$.

\subsubsection{{\color{TUMdred}Velocity}-Based Grasping Strategy}\label{sec:Grasp:approach:robot-control:velocity}
This
strategy directly generates a Cartesian velocity profile from the estimated alignment error.
Thus, there is no need for an additional \ac{FT}-sensor.
Instead, we argue that 
it is beneficial to directly constrain the velocity to a maximum impact velocity $\vv{v}[\max]$.
If further the update time $\delta_t$ is sufficiently high, 
the Cartesian robot motion-dynamics in $\toolFr$ can be approximated as a
linear point-mass $\vx[\toolFr]\Tp \approx \vxt[\toolFr] + \vu[\toolFr]\Tt \delta_t$,
such that the currently estimated pose-error from~\autoref{sec:Grasp:approach:alignment-estimation}
can be decreased by solving the~\ac{MPC}-problem
\begin{equation}\label{eq:Grasp:approach:vel-ctrl}
\begin{aligned}
    \min_{\scatraj{\vu[\toolFr]}[t][{T_{\max}}]} &
        \sum_{t=0}^{T_{\max}}
            {\sca[\toolFr]{\gls{ErrorVec}}\Tt}\tr\Mat{C}[Q]{\sca[\toolFr]{\gls{ErrorVec}}\Tt} +
            {\vu[\toolFr]}\Tt\tr \Mat{C}[R] {\vu[\toolFr]}\Tt
            \\
        \subjectto \quad&
        \sca[\toolFr]{\gls{ErrorVec}}\Tp = \Mat{A} \sca[\toolFr]{\gls{ErrorVec}}\Tt + \Mat{B} \vu[\toolFr]\Tt\\
        &-\vv{v}[\max] \leq \vu[\toolFr]\Tt  \leq \vv{v}[\max]
\end{aligned}
\end{equation}
with
$\Mat{A}=\ones[6][6]$ and $\Mat{B}=\ones[6][6] \delta_t$ due to the locally linearized model.

\subsubsection{Overall Controller Implementation}\label{sec:Grasp:approach:robot-control:implementation}
\begin{figure}[t]
    \vspace{0.3cm}
    \centering
         \begin{center}
         \resizebox {\linewidth}{!} {\begin{tikzpicture}
         \input{tikz/wsg_styles}
         \input{tikz/HybridSnsCtrlWSG.tex}
      \end{tikzpicture}}
    \end{center}
    \CompactCaption{
            Schematic overview of the proposed grasping strategies 
            ({\color{TUMdred}velocity}-/{\color{TUMdgreen} force}-based). Noisy blue boxes
             denote hardware components. Desired values have been omitted for brevity.
      }
    \vspace{-0.45cm}
    \label{fig:Grasp:approach:sns-hybrid-ctrl}
\end{figure}
Given the proposed grasping strategies from above, we now outline how the
overall grasping controller is implemented 
as 
sketched in~\autoref{fig:Grasp:approach:sns-hybrid-ctrl} in three layers.
The upper layer denotes the aligment error-estimation from
\autoref{sec:Grasp:approach:alignment-estimation}, 
while the bottom layer shows the industrial robot platform, which
receives Cartesian \ee/-velocity-commands $\vu$.
Assuming that the control-cycle is run at a sufficiently high update-rate,
it is possible to design a hybrid foce-velocity controller
\begin{equation}\label{eq:Grasp:approach:sensor-track:hybrid-sns-ctrl}
\begin{aligned}
\vu :=
\sca{\gls{SelectionMat}}[\RotMat][\vx]
    \vxt[\baseFr][\eeFr\des]
    + \sca{\gls{SelectionMat}}[\RotMat][\Fx] \GainMat[P][\Fx] \sca{\gls{ErrorVec}}[][\Fx]
\end{aligned}
\end{equation}
to the robot, using the selection-matrices
from~\eqref{eq:Grasp:approach:force-control:selection-matrix}.
Thus, the controller either follows a Cartesian velocity-profile $\vxt[\baseFr][\eeFr\des]$
or a force-profile $\Fxd$, that minimizes wrench-error $\sca{\gls{ErrorVec}}[][\Fx]$
from~\eqref{eq:Grasp:approach:force-control:PI}.
This controller differs from classic hybrid force-position control by the fact
that disabling one control-modality does not directly result in switching to the alternative modality.
Instead if $\sca{\gls{SelectionElem}}[k][\vx] = \sca{\gls{SelectionElem}}[k][\Fx] = 0$ holds,
the internal position-control loop of the industrial robot becomes active.
Nonetheless, for a correct decoupling of the individual control policies,
the selection matrices need to hold
$\sca{\gls{SelectionElem}}[k][\vx] \sca{\gls{SelectionElem}}[k][\Fx] = 0$.
Thus, by limiting each element in~\eqref{eq:Grasp:approach:force-control:selection-matrix}
to solely one grasping strategy, the control outputs of the individual component
can directly be inserted in~\eqref{eq:Grasp:approach:sensor-track:hybrid-sns-ctrl} by 
transforming $\vu[\toolFr][{\color{TUMdred}v}]$ to $\vu[\baseFr][\eeFr, {\color{TUMdred}v}]$
and replacing the P-control in~\eqref{eq:Grasp:approach:sensor-track:hybrid-sns-ctrl}
by~\eqref{eq:Grasp:approach:force-control}.

\subsection{Extending to Complex Grippers}\label{sec:Grasp:approach:multi-finger}
Even though the method sketched in this section has been outlined explicitly
for a parallel gripper,
\ie/,~an alignment error estimation from two~\acp{DSA},
the presented method is not limited to this configuration.
As both control-strategies in this work solely rely on
the estimated alignment error, they are not directly dependent
on the number of fingers in use, as long as the number of fingers
does not distinctly decrease the update rate of the alignment error-estimation.
In fact their performance solely relies on the accuracy of the 
alignment-error estimation.
The method presented in~\autoref{sec:Grasp:approach:alignment-estimation}
can be extended to multiple fingers
by evaluating the pressure distribution or \acp{CoP}
of each finger against a desired set-value.
Thus, comparing the~\ac{CoP} of each finger against the
desired~\acp{CoP}, results in an alignment error
per finger.
While minimizing the mean of these alignment errors replicates the
procedure presented in this work, having access to multiple~\ac{CoP} measurements
would allow to use alternative methods, \eg/,~weighted mean.
As this evaluation requires empirical evidence, we leave this for future work.
