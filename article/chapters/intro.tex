\section{Introduction}\label{sec:Grasp:introduction}

In the context of sensitive grasping, a robot is required to react in a compliant
manner in order to limit interaction wrenches between the robot gripper and the
dedicated object.
Thus, a majority of approaches rely on compliant manipulators, fingers or tool-changers
that allow to directly adjust for unforeseen impacts,
or seek for a sufficiently accurate perception framework.
Nonetheless, few examples allow an adaptation of the grasp  when neither
perception is accurate nor a compliant robot platform is available.
Thus, this work tackles the issue of sensitive grasping
for conventional industrial robot control interfaces -- \ie/,~stiff position-controlled manipulators -- without relying on external camera or depth-sensor data.
\\\noindent
Instead, we claim that it is beneficial to equip a robot with pressure sensitive fingers
using~\acp{DSA}.
Given the spatial resolution of these~\acp{DSA} rather than solely the force observed on
each finger motor or the~\ac{FT}-sensor directly,
we outline how a robot can estimate the alignment error during initial grasp trials.
\\\noindent
In order to evaluate this approach empirically,
we equip an industrial robot platform, a COMAU Racer 5 0.80,
with a two-finger parallel gripper - a WSG 50 -
that provides a~\ac{DSA} on each finger as shown in~\autoref{fig:Grasp:robot}.
Besides extending the current~\ac{ROS}-driver with~\ac{DSA} support,
the contribution of this work proposes an efficient gripper alignment error estimation.
Furthermore, we outline how a hybrid force-velocity control architecture
can be used to decrease the estimated alignment error, while keeping
interaction wrenches of the robot and the environment limited.
The novel grasping controller is eventually 
evaluated on exemplary 
grasping tasks,
where the goal-pose is noisy and needs to be compensated 
solely 
from~\ac{FT} and~\ac{DSA} measurements.
\begin{figure}[t]
    \centering
    \resizebox{\linewidth}{!}{
        \begin{tikzpicture}
            \input{tikz/wsg_styles}
            % \input{tikz/fad_styles}
            \input{tikz/adative_grasp_main}
        \end{tikzpicture}
        }
    \CompactCaption{
        Adaptive grasping strategy using tactile sensor data
        to send hybrid control-strategies along selective axes
        to an industrial robot via {\color{TUMdred}{velocity-based}}
        or {\color{TUMdgreen}{force-based}} grasping strategies.
    }\label{fig:Grasp:robot}
    \vspace{-0.4cm}
\end{figure}
\\\noindent
Below we sketch the contribution of this article in relation to related work
and outline the notation used in 
this work.
We proceed with a mathematical formulation of the problem
in~\autoref{sec:Grasp:problem-description}
and outline the presented grasping controller
in~\autoref{sec:Grasp:technical-approach}.
We conclude this work with an empirical evaluation
in \autoref{sec:Grasp:experiment}, and an overall conclusion
in~\autoref{sec:Grasp:conclusion}.

\subsection{Related Work}\label{sec:Grasp:related-work}

In the context of adaptive grasping, a variety of approaches have focussed on
developing novel robot grasping grippers
(\cite{
    9115995, 
    6630954,
    8009480,
    doi:10.1177/0278364917714062, 
    doi:10.1177/1729881418766783, 
    krut2005force, 
    koustoumpardis2004devices%
}),
such as tendon-driven mechanics~\cite{dollar2010contact} or low impedance~\cite{natale2006sensitive} fingers,
These hardware designs have shown great results in improving robot capabilities,
yet these systems are often costly and are not as mechanically robust and broadly available
as off-the-shelf industrial grippers.
Thus, the majority of related work focused on increasing 
robotic capabilities using off-the-shelf grippers.
In here, several approaches have applied
\ac{ML}-techniques~(\cite{5979644, 7487351, doi:10.1177/1729881416682706, 7487517}),
where the environment is mainly perceived via a camera, but also force-based
approaches have been 
introduced~\cite{8793733, DBLP:conf/iros/SteffenHR07, DBLP:journals/arobots/DangA14} that rely on tactile feedback.
These approaches show great performance but usually require tremendous amount of data.
Similarly, planning-based methods have been outlined for automated grasping.
These decision-theoretic concepts range from
partially observable Markov decision processes~(\cite{4209819, Garg2019LearningTG}),
heuristic planning strategies~(\cite{DBLP:conf/iser/LeeperHCS10, DBLP:conf/iros/HsiaoCCJ10}),
visual servoing~(\cite{rusu2009perception,% <- Beetz Michael
                      saxena2008robotic}% <- Andrew Ng
                      )
specialized sensors~\cite{DBLP:conf/icra/HsiaoNHSN09},
or online object refinement and adjustment~\cite{DBLP:conf/icra/DragievTG13}.
Regarding the aspect of tactile sensors, previous work has outlined how grasping robustness
can be improved by using tactile
sensors~(\cite{5759756, 7363558}),
where the major emphasis is set on~\ac{ML} again or the
environment is assumed to be compliant.
The application of tactile fingers range from six-axes force-torque sensors \cite{eberman1994application},
stress rate sensors and acceleration sensors~\cite{howe1990grasping}, 
biomimetic tactile sensors with a weakly conductive fluid \cite{wettels2008biomimetic} to 
optical force measurements \cite{ward2018tactip}.
The concept of~\ac{DSA} was proposed by Romano \etal/~\cite{romano2011humangrasping}
that shall mimic mechanoreceptive afferents in glabrous (i.e. non-hairy)
human skin~\cite{johansson2009coding}.
A tactile sensor array unit that does not only measure normal forces but also shear forces,
was developed by Kis \etal/~\cite{kis2006grasp}.
As these approaches have focused on detecting events,
rather than controlling the motion of a robot
based on the~\ac{DSA} feedback, 
we focus on the concept of versatile manipulation with simple
grippers~(\cite{mason2011generality,mason2012autonomous}).
Thus, we seek to improve robot grasping skill-sets by deriving a
grasping strategy exploiting the~\ac{DSA} readings.
\\\noindent
In this context the contribution of this article is given as
\begin{enumerate}[leftmargin=15pt]
    \item outlining a novel grasping alignment error estimation based on
          tactile sensor readings without relying on additional~\ac{FT}-data.
    \item a publicly available~\ac{ROS}-driver implementation with support for the
          tactile WSG 50~\ac{DSA} fingers\footnote{
              available at~\url{https://gitlab.com/VGab/ros-wsg-50}
              }
          with a communication rate of up to \SI{5.49}{\hertz} during contact.
    \item defining novel grasping strategies that exploit the current
        alignment error estimation and drive a robot to the desired
        goal pose while limiting the interaction wrenches between robot and object.
\end{enumerate}

\subsection{Notation}\label{sec:Grasp:notation}
Using an arbitrarily set placeholder variable \gls{Dummy},
we denote vectors as $\gls{VecNot} \in\RE[n]$ and matrices as $\gls{MatNot}\in\RE[m][n]$.
The identity vector and identity matrix are denoted as \gls{OneVecNot} and
\gls{OneMatNot}, and similarly as \gls{ZeroVecNot} and \gls{ZeroMatNot}
as the zero vector and zero matrix.
A temporal sequence of vectors \gls{VecNot} over time is described as
a trajectory $\gls{TrajNot}:=\tuple{\sseqcom{\gls{VecNot}}{\tm}{1}{2}{T}}$.
Indexing over time is denoted as \gls{TimeIdxNot}.
Referring to a list, vector, \etc/~we denote \gls{ContentIterNot}
as the scalar element at location $i$.
Explicit elements of matrices or vectors are denoted as \gls{MatElemNot}.
For kinematic robot chains, we refer to the origin of the chain as base $\baseFr$,
the \ee/~as $\eeFr$, and the tool as $\toolFr$.
Coordinate transformation matrices are denoted as $\Transform{{\color{TUMblue}\mathfrak{i}}}{{\color{TUMblue}\mathfrak{j}}}$
as a transformation
 from ${\color{TUMblue}\mathfrak{i}}$ to ${\color{TUMblue}\mathfrak{j}}$.
We denote \gls{RRoll},\gls{RPitch} and \gls{RYaw} as the rotation matrices
around coordinate axes \gls{rviz_x}, \gls{rviz_y} and \gls{rviz_z}.
Regarding translational notations,
$\sca[\baseFr]{\gls{VecNot}}[\eeFr\toolFr]$ describes a vector
$\gls{VecNot}$ pointing from the \ee/~to the tool, expressed in the base-frame.
If no explicit reference frame is provided, the variable is given \wrt/~$\baseFr$.

